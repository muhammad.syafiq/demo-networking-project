package com.kotlin.demo.networking.project.activity.main

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MainDatasource {
    @GET("3/discover/movie")
    fun getMovie(
        @Query("api_key") key: String
    ): Single<MainResponse>
}