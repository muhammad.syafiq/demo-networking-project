package com.kotlin.demo.networking.project.activity.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.kotlin.demo.networking.project.BuildConfig
import com.kotlin.demo.networking.project.R
import kotlinx.android.synthetic.main.adapter_movie.view.*

class MainAdapter(
    private val results: List<Result>
): RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder =
        MainViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.adapter_movie, parent, false)
        )

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(results[position])
    }

    override fun getItemCount(): Int = results.size

    inner class MainViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(data: Result) {
            with(itemView) {
                tvTitle.text = data.title
                tvOverview.text = data.overview
                ivCover.load("${BuildConfig.IMAGE_URL}/${data.posterPath}")
            }
        }
    }
}