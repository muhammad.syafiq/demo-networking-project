package com.kotlin.demo.networking.project.activity.main

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

data class MainEntity (
    val res1: List<Result>,
    val res2: List<Result>
)

@Serializable
data class MainResponse (
    val results: List<Result>
)

@Serializable
data class Result(
    val title: String,
    val overview: String,
    @SerialName("poster_path")
    val posterPath: String
)