package com.kotlin.demo.networking.project.activity.main

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.kotlin.demo.networking.project.BuildConfig
import com.kotlin.demo.networking.project.R
import com.kotlin.demo.networking.project.network.NetworkProvider
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var mainEntitiy: Single<MainEntity>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = getString(R.string.app_name)

        val datasource = NetworkProvider.providesRetrofit().create(MainDatasource::class.java)
        val datasource2 = NetworkProvider.providesRetrofit().create(MainDatasource::class.java)

        mainEntitiy = Single.zip(
            datasource.getMovie(BuildConfig.KEY),
            datasource2.getMovie(BuildConfig.KEY),
            BiFunction { t1, t2 ->
                MainEntity(t1.results, t2.results)
            }
        )

        mainEntitiy
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                rv_movie.adapter = MainAdapter(it.res1) //source satu
                Log.d(MainActivity::class.java.simpleName, it.res2.toString()) //source dua
            },
                {
                    Log.d(MainActivity::class.java.simpleName, it.message)
                }).addTo(CompositeDisposable())

//        mainEntitiy?.subscribe ({
//            rv_movie.adapter = MainAdapter(it.res1)
//        }, {
//            Log.d("Err", it.message)
//        })?.addTo(CompositeDisposable())

//        datasource.getMovie(BuildConfig.KEY)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(object : DisposableSingleObserver<MainResponse>() {
//                override fun onSuccess(t: MainResponse) {
//                    t.results.let { result ->
//                        rv_movie.adapter = MainAdapter(result)
//                    }
//                }
//
//                override fun onError(e: Throwable) {
//
//                }
//            })
    }
}