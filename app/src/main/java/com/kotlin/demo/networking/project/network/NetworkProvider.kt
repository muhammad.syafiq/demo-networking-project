package com.kotlin.demo.networking.project.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.kotlin.demo.networking.project.BuildConfig
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

object NetworkProvider {
    private val contentType = MediaType.get("application/json")

    @kotlinx.serialization.UnstableDefault
    fun providesRetrofit(): Retrofit = Retrofit.Builder().apply {
        client(providesHttpClient())
        baseUrl(BuildConfig.BASE_URL)
        addConverterFactory(Json.nonstrict.asConverterFactory(contentType))
        addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }.build()

    private fun providesHttpClient(): OkHttpClient =
        OkHttpClient.Builder().apply {
            retryOnConnectionFailure(true)
            addInterceptor(providesHttpClientInterceptor())
        }.build()

    private fun providesHttpClientInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            level = when (BuildConfig.DEBUG) {
                true -> HttpLoggingInterceptor.Level.BODY
                false -> HttpLoggingInterceptor.Level.NONE
            }
        }
}